/*
 * UNIVERSIDADE CATÓLICA DOM BOSCO
 * TRABALHO DE ESTRUTURA DE DADOS II
 * PROFESSOR: MARCOS ALVES
 */
package algkmp;
import javax.swing.JOptionPane;
/**
 * @data de início 15/05/2017 
 * @data de término 16/05/2017 
 * @author MILAD ROGHANIAN RA160985
 */
public class Interface {
    
    public static void main(String args[]) throws CloneNotSupportedException {
        KMP kmp = new KMP();
        Controle control = new Controle();
        boolean executar=true;
        int op;
        
        while (executar)
        {
            kmp.setFrase(JOptionPane.showInputDialog("Digite a Frase:"));
            while (kmp.getFrase().length()==0){ //enquanto a entrada não for nula
                kmp.setFrase(JOptionPane.showInputDialog(null,"TEXTO VAZIO, DIGITE NOVAMENTE","ATENÇÃO",JOptionPane.WARNING_MESSAGE));
            }
            kmp.setBusca(JOptionPane.showInputDialog("Digite o padrão a buscar:"));
            while (kmp.getBusca().length()==0){ //enquanto a entrada não for nula
                kmp.setBusca(JOptionPane.showInputDialog(null,"PADRÃO VAZIO, DIGITE NOVAMENTE","ATENÇÃO",JOptionPane.WARNING_MESSAGE));
            }
            int aux = control.algkmp(kmp.getFrase(), kmp.getFrase().length(),kmp.getBusca(),kmp.getBusca().length());
        
            if (aux>-1){ //se a função não retornar -1
                JOptionPane.showMessageDialog(null,"O PADRÃO FOI ENCONTRADO NA POSIÇÃO " +aux+" INICIADO COM: "+kmp.getFrase().charAt(aux-1),"MENSAGEM",JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null,"PADRÃO NÃO ENCONTRADO","MENSAGEM",JOptionPane.WARNING_MESSAGE);
            }
            
            Object[] principal = {"SIM", "NÃO" };
            
            op = JOptionPane.showOptionDialog(null, "DESEJA EXECUTAR NOVAMENTE?", "MENSAGEM", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, principal, principal[0]);
            if (op==1){ //Se nao
                System.exit(0);//termina de executar
            }
        }
    }
}
