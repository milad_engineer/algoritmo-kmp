/*
 * UNIVERSIDADE CATÓLICA DOM BOSCO
 * TRABALHO DE ESTRUTURA DE DADOS II
 * PROFESSOR: MARCOS ALVES
 */
package algkmp;
/**
 * @data de início 15/05/2017 
 * @data de término 16/05/2017 
 * @author MILAD ROGHANIAN RA160985
 */
public class KMP {
    private String frase = new String();//frase usada na entrada
    private String busca = new String();//padrão a se buscar na frase
    

    /**
     * @return the frase
     */
    public String getFrase() {
        return frase;
    }

    /**
     * @param frase the frase to set
     */
    public void setFrase(String frase) {
        this.frase = frase;
    }
    
    /**
     * @return the frase
     */
    public String getBusca() {
        return busca;
    }

    /**
     * @param frase the frase to set
     */
    public void setBusca(String busca) {
        this.busca = busca;
    }
}
