/*
 * UNIVERSIDADE CATÓLICA DOM BOSCO
 * TRABALHO DE ESTRUTURA DE DADOS II
 * PROFESSOR: MARCOS ALVES
 */
package algkmp;
import java.util.ArrayList;
import javax.swing.JOptionPane;
/**
 * @data de início 15/05/2017 
 * @data de término 16/05/2017 
 * @author MILAD ROGHANIAN RA160985
 */
public class Controle 
{
    int[] saida;//vetor com as posições usado na função prefixo
    
    //######### FUNÇÃO DE TRATAMENTO DO PADRÃO ###########
    public void prefixo (String p, int m){
        int i=0, j=0;
        this.saida = new int[m];
        while (i<m)
        {
            if(p.charAt(i)==p.charAt(j)){
                this.saida[i] = j+1;
                i++;
                j++;
            } else if (j>0) {
                j = this.saida[j-1];
            } else { 
                this.saida[i] = 0;
                i++;
            }
        } 
    }
    
    //######### FUNÇÃO DE BUSCA DO PADRÃO NA FRASE ###########
    public int algkmp (String t, int n, String p, int m){
        prefixo(p,m);
        int i=0, j=0;
        while (i<n && j!=m)
        {
            if(p.charAt(j)==t.charAt(i)){
                i++;
                j++;
            } else if (j>0) {
                j=this.saida[j-1];
            } else {
                i++;    
            }
        }
        if(j==m){
            return i-m+1;  
        } else { 
            
            return -1;
        }
    }
}
